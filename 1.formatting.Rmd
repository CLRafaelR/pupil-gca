# data formatting

## import packages

```{r}
library(reshape)
library(dplyr)
library(magrittr)
"%+%"  = function(a,b) paste0(a,b)
```

## ファイルリスト

* 任意のディレクトリの
    * 'data/tsv/'にデータは置いてある
* 任意のパターンを持つファイルを
    * Tobii Studio からのデータは以下のフォーマット
        * `<project名>_<test名>_<被験者名>_<segment名>.tsv`
        * 今回の `<project名>_<test名>` は `npi_2017_New test` 
* リスト化
    * 一気に操作できる。

```{r}
pattern2list = function(file_pattern,path=''){
    file_pattern %>%
        list.files(path=path) %>% 
        as.list ->
    data_list
    if (length(data_list) == 0){
        stop('check wd and file pattern')
        return(FALSE)
    }else{
        message("loaded file list!")
        return(data_list)
    }
}

path='data/tsv/'
file_pattern = "npi_2017_New test"
data_list = file_pattern %>% pattern2list(path)
file_pattern %>%  pattern2list(path) %>% length
```

## 瞳孔の情報が入っているデータのみのリストを作成

```{r}
filter4pupil = function(data_list,path=''){
    filtered_list = as.list(NULL)
    for(file_name in data_list){
        file_name = path %+% file_name
        file_name %>% 
            read.table(head=T, sep="\t", na.string="NA", encoding="UTF-8") %>%  
            filter(!(PupilLeft %>% is.na)) ->
        trial
        if(nrow(trial) == 0){
            message("Bad trial: " %+% file_name)
        }else{
            filtered_list %<>% append(file_name)
        }
    }
    if (length(filtered_list) == length(data_list)){
        message("there's no bad trial!")
    }
    return(filtered_list)
}

path='data/tsv/'
file_pattern = "npi_2017_New test"
data_list = file_pattern %>% pattern2list(path) %>% filter4pupil(path)
file_pattern %>%  pattern2list(path) %>% filter4pupil(path) %>% length
```

## 全員分のデータを一つのファイルにまとめる

1. ファイル名からdata.frameを取得
1. data.frameから修正したdata.frameを取得
1. StudioEventDataの隣E-prime情報をマッピング

### ファイル名からdata.frameを取得

```{r}
fn2df = function(file_name, path='', head=T){
    file_name = path %+% file_name
    file_name %>%  
        as.character() %>%  
        read.table(head=head,sep="\t",
                   na.string="NA",
                   encoding="UTF-8") %>%
        as.data.frame %>%
    return
}
# 適用例
file_name = data_list[1] %>% print
file_name %>% fn2df() %>% head %>% View
```

### data.frameから修正したdata.frameを取得

```{r}
# see the raw_df before explain this function
refiner = function(raw_df){
    raw_df %>% 
        select(subject, SegmentName,
               SegmentStart, RecordingTimestamp, PupilLeft) %>%
        mutate(Timestamp = RecordingTimestamp - SegmentStart) %>%
        select(subject, SegmentName,
               Timestamp, PupilLeft) %>%
        filter(!(PupilLeft %>% is.na())) %>% 
    return
    }

path='data/tsv/'
file_name = data_list[1] %>% print
file_name %>% fn2df() %>% refiner %>% head %T>% View
```

### E-prime情報を区切ってbase_data_frameの右側に(StudioEventDataの隣から)マッピング

* matrix()関数を使って、一列しかなかった list を8ずつ切って横にならべ、
* base_data_frame と同数の行を持つ data_frame にします。

```{r}
addStudioEventData = function(base_data_frame, file_name, numcol,path='') {
    raw_data_frame = fn2df(file_name,path)
    raw_data_frame[1,]$StudioEventData %>%
        as.character %>%
        strsplit(" ")%>%
        unlist() ->
    list_of_eventdata
    if (length(list_of_eventdata)!=numcol){
        warning("Bad trial: " %+% file_name )
    }
    list_of_eventdata %>% 
        matrix(nrow=nrow(base_data_frame),
               ncol=length(list_of_eventdata), byrow=T) %>% 
        as.data.frame %>% 
        select(V1,         V2,       V3,      V4) %>% 
        rename(blocklist=V1, order=V2, item=V3, condition=V4) ->
    eventdata_data_frame
    data_with_eventdata = cbind(base_data_frame, eventdata_data_frame)
    return(data_with_eventdata)
    }

file_name %>% fn2df() %>% refiner %>% head %T>% View
file_name %>% fn2df() %>% refiner %>% addStudioEventData(file_name,8) %>% head %T>% View
```

## 実行部分

```{r, echo = FALSE}
path='data/tsv/'
file_pattern = "npi_2017_New test"
data_list = file_pattern %>% pattern2list(path) %>% filter4pupil(path)

output ="./data/output.tsv" 
getwd()
num_eventcol = 8

for(i in 1:length(data_list)){
    file_name=data_list[i]
    print("now access to: "%+% file_name)
    file_name %>% as.character() %>% 
        fn2df %>% refiner %>% 
        addStudioEventData(file_name,num_eventcol) ->
    data_frame
    # if it is the first file(i==1), initialize(append=F) and make colname(col.name=T)
    # seems like i/o on /tmp faster than /home/thesis/pupil/data
    write.table(data_frame,output,
                append=!(i==1),sep="\t",
                row.names=F,col.names=(i==1))
}


output %>% fn2df %>% head %>% View
# you should make bins
```

Binnig time course data for

1. formatting data
1. avoiding false positive results
1. make data smaller

the optimal approaches to make bins vary widely depending on
1. nature of the data
1. research domain
1. research question

outlier
* you can assume the width

```{r}
output ="./data/output.tsv" 
data_all = output %>% fn2df
data_all %>% head %>% View()

summary(data_all)

pupil_data <- 
    data_all %>% 
    dplyr::rename(subject=ParticipantName,
           stamp=Timestamp,
           pupil=PupilLeft) %>% 
    mutate(blocklist=NULL,
           SegmentName=NULL,
           order=as.numeric(order),
           subject = as.factor(subject),
           item = as.factor(item),
           Particle=as.factor(ifelse(condition=="a"|condition=="c", "NPI", "Non-NPI")),
           Negation=as.factor(ifelse(condition=="a"|condition=="b", "Global", "Local")))

head(pupil_data) %>% View()
```

We have to make bins how many observations we get?
* want to average pupil size
* for each 50ms

1. count the rows in a bin using `hist`
    * how many rows are in the bin
    * (e.g. from 0 to 50, there are 8 rows)
    * if we mean the rows, the number represent the rows from 0 to 50
    

```{r}
bin=50

# take a list of timestamp and bin, return the same number of rows with binned stamps 
binner = function(stamp,bin){
    mapply(rep, 
           (hist(stamp, breaks=seq(0,ceiling(max(stamp)/bin)*bin, bin), plot=FALSE) %$% breaks)[-1],
           (hist(stamp, breaks=seq(0,ceiling(max(stamp)/bin)*bin, bin), plot=FALSE) %$% counts)
    ) %>% unlist() %>% return
}

# https://rpubs.com/gg_hatano/17944
binned_data = pupil_data %>% group_by(subject,item) %>%
    do(data.frame(bins=binner(.$stamp,bin=50))) %$% 
    cbind(bins,arrange(pupil_data,subject,item)) %>%
    group_by(subject, item, bins, order, condition, Particle, Negation) %>% 
    summarise(pupilsize=mean(pupil)) %>%  as.data.frame()

summary(binned_data)
head(binned_data) %>% View()

save(binned_data, file="./data/binned_data.Rdata")
```

you can check how it works

```{r}
pupil_data %>% filter(subject=="P22",item==6, stamp>=0, stamp<=50) %$% mean(pupil)
binned_data %>% dplyr::filter(subject=="P22",item==6) %>% head %>% View()
```

breaks for `hist` need to span range of max(stamp)

```{r}
p05 %>% filter(item==1) %$% max(stamp)/bin
p05 %>% filter(item==1) %$% ceiling(max(stamp)/bin)
p05 %>% filter(item==1) %$% ceiling(max(stamp)/bin)*bin
p05 %>% filter(item==1) %$% max(stamp)
```